const { createLogger, format, transports } = require('winston');
const path = require('path')
const config = require('config');
require('winston-daily-rotate-file');

const loggerConfig = config.get('config.logger');

// initializing winston logger 
const logger = createLogger({
    level: 'silly',
    transports: [
        // below transport is for logging in terminal
        new transports.Console({
        handleExceptions: loggerConfig.handleExceptions,
        format: format.combine(
            format.colorize(),
            format.timestamp({
                format: loggerConfig.timestampInLogs
            }),
            format.label({ label: path.basename(process.mainModule.filename) }),
            format.printf(info => `${info.timestamp}(${info.label}) ${info.level} : ${info.message}`)
        )
    }),

    // below transport is for creating log files
    new transports.DailyRotateFile({
        filename: path.join(loggerConfig.logsFolder,`%DATE%-results.log`),
        maxSize: loggerConfig.maxSize,
        format: format.combine(
            format.timestamp({
                format: loggerConfig.timestampInLogs
            }),
            format.label({ label: path.basename(process.mainModule.filename) }),
            format.printf(info => `${info.timestamp}(${info.label}) ${info.level} : ${info.message}`)
        ),
        datePattern: loggerConfig.dateFormatInFileName,
        handleExceptions: loggerConfig.handleExceptions,
        zippedArchive: loggerConfig.zippedArchive
    })]
});

// common logger function
let logUtil = (type="info",message='', input='') => {
    if(typeof input == 'object') {
        input=JSON.stringify(input)
    }
    logger.log(type, message+''+input);
}

module.exports = logUtil