const logger = require('../utils/logger');
const service = require('../services/appservice');

const controllerList = {
    initialRoot: (req, res, next) => {
        res.status(200).json({"name":service.initialRootService('app1')})
        logger("info", "End of Request")
    },

    secFunc: function(req, res, next) {
        res.status(200).json({"sec":true})
    }
}

module.exports = controllerList