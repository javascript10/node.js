const express = require('express')
const logger = require('../utils/logger')

const router = express.Router()
const controller = require('../controllers/appController')

// middleware that is specific to this router
router.use((req, res, next) => {
    logger("info", `${req.method} API request received for `, req.originalUrl);
    next();
})

router.get('/', controller.initialRoot)

module.exports = router