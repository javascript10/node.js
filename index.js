const express = require('express')
const config = require('config');
// const bodyParser = require('body-parser')

let logger = require('./utils/logger');
let routes = require('./routes/routes')

const appConfig = config.get('config.app');
const app = express()

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// we can use body-parser or express inbuit parser
/* 
app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({ extended: true }));
 */

app.use('/app', routes)
app.listen(appConfig.port, () => {
    logger('info',`${appConfig.name} is running on http://localhost:${appConfig.port}`)
})